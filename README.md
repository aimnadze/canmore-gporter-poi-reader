Canmore G-Porter POI Reader
===========================
A PHP program to read points of interests (POIs)
recorded with "Canmore G-Porter GP-102+".
The program read files located at `CANMORE/GP-102/POIs`
on the device and outputs the data in standard output.

Usage
-----
```
$ ./program.php <file1> [<file2> ...]
```
